(function() {
	const $liveChat 		= document.querySelector('#liveChat');
	const $isTyping 		= document.querySelector('.chat-container__typing');
	const $messageInput = document.querySelector('#chatInput');
	const dateOptions 	= {
		weekday: 'short',
		year: 'numeric',
		month: 'short',
		day: 'numeric',
	};
	let message, date, time, from;
	$messageInput.value = '';
	
	const loadAndAppendData = (chats) => {
		for (const chat of chats) {
			const { from } = chat;
			extractChatPropsAndSetVars(chat);
			$liveChat.appendChild(createMessageContainer(from.toLowerCase()));
			$liveChat.scrollIntoView(false);
		}
	};

	const sendMessage = (event) => {
		event.preventDefault();
		if (event.code === 'Enter') {
			const msg = $messageInput.value.trim();
			if (!msg) {
				$messageInput.value = '';
				return;
			}
			if (msg.match(".*[%<>&;'\0-].*")) {
				$messageInput.value = $messageInput.value.trim();
				return;
			}
			chat.sendChat(msg);
			$messageInput.value = '';
			$liveChat.scrollIntoView(false);
			$isTyping.classList.add('show');
		}
	};

	const getAndAppendLatestChat = ({ chat }) => {
		const { from } = chat;
		extractChatPropsAndSetVars(chat);
		$liveChat.appendChild(createMessageContainer(from.toLowerCase()));
		$liveChat.scrollIntoView(false);
		$isTyping.classList.remove('show');
	};

	const extractChatPropsAndSetVars = ({ datetime, ...chat }) => {
		({
			message,
			from,
			date = new Date(datetime).toLocaleDateString('en-US', dateOptions),
			time = new Date(datetime).toLocaleTimeString('en-US'),
		} = chat);
	};

	const createMessageContainer = (className) => {
		const $div 		 = document.createElement('div');
		$div.className = 'chat-container__chat-box';
		$div.innerHTML = `
			<div class='chat-container__${className} chat-container--bubble'>
				<p class='chat-container--message'>${message}</p>
			</div>
			<div class='chat-container__message-${className}'>
				<span class='chat-container__meta-${className}'>${from}</span>
				<span class='chat-container__meta-${className}'> at ${time} on ${date}</span>
			<div>
		`;
		return $div;
	};

	chat.getChatHistory(loadAndAppendData);
	chat.addListener('chatreceived', getAndAppendLatestChat);
	$messageInput.addEventListener('keyup', sendMessage, false);
}());